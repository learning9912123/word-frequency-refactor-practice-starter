import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    public String getResult(String inputStr) {

        String[] words = inputStr.split("\\s+");
        try {
            List<WordFrequency> wordFrequencyList = buildWordFrequencyList(words);

            List<WordFrequency> wordFrequencies = groupWordFrequencyByWord(wordFrequencyList);

            return buildResult(wordFrequencies);

        } catch (Exception e) {
            throw new CalculateException();
        }
    }

    private List<WordFrequency> buildWordFrequencyList(String[] words) {
        List<WordFrequency> wordFrequencyList = new ArrayList<>();
        for (String word : words) {
            WordFrequency wordFrequency = new WordFrequency(word, 1);
            wordFrequencyList.add(wordFrequency);
        }
        return wordFrequencyList;
    }

    private List<WordFrequency> groupWordFrequencyByWord(List<WordFrequency> wordFrequencyList) {
        Map<String, Integer> sameWordmap = wordFrequencyList.stream()
                .collect(Collectors.groupingBy(WordFrequency::getWord, Collectors.summingInt(WordFrequency::getWordCount)));

        return sameWordmap.entrySet().stream()
                .map(entry -> new WordFrequency(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }


    private String buildResult(List<WordFrequency> sameWordList) {
        sameWordList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());

        return sameWordList.stream()
                .map(word -> word.getWord() + " " + word.getWordCount())
                .collect(Collectors.joining("\n"));
    }


}
