public class CalculateException extends RuntimeException{
    public CalculateException() {
        super("Calculate Error");
    }
}
